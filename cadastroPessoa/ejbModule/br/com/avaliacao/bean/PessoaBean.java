package br.com.avaliacao.bean;

import java.util.Collection;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.avaliacao.entity.Pessoa;

@Stateless
@LocalBean
public class PessoaBean implements PessoaBeanLocal {

	@PersistenceContext(unitName = "ExampleDS")
	private EntityManager entityManager;
	
    public PessoaBean() {
        
    }

	@Override
	public boolean adicionarPessoa(Pessoa Pessoa) {
		entityManager.persist(Pessoa);
		return true;
	}

	@Override
	public Collection<Pessoa> listarPessoaes() {
		Query query = entityManager.createQuery("SELECT o FROM Pessoa o");
	    return (Collection<Pessoa>) query.getResultList();
	}

	@Override
	public Pessoa listarPessoa(Integer id) {
		return entityManager.find(Pessoa.class, id);
	}

	@Override
	public boolean editarPessoa(Pessoa pessoa) {
		entityManager.merge(pessoa);
		entityManager.flush();
		return true;
	}

	@Override
	public boolean excluirPessoa(Pessoa pessoa) {
		entityManager.remove(pessoa);
		return true;
	}

}
