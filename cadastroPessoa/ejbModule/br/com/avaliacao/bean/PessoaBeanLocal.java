package br.com.avaliacao.bean;

import java.util.Collection;

import javax.ejb.Remote;

import br.com.avaliacao.entity.Pessoa;

@Remote
public interface PessoaBeanLocal {

	public boolean adicionarPessoa(Pessoa Pessoa);
	
	public Collection<Pessoa> listarPessoaes();
	
	public Pessoa listarPessoa(Integer id);	
	
	public boolean editarPessoa(Pessoa Pessoa);
	
	public boolean excluirPessoa(Pessoa Pessoa);
}
