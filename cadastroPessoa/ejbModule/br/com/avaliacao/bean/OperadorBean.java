package br.com.avaliacao.bean;

import java.util.Collection;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.avaliacao.entity.Operador;

@Stateless
@LocalBean
public class OperadorBean implements OperadorBeanLocal {

	@PersistenceContext(unitName = "ExampleDS")
	private EntityManager entityManager;
	
    public OperadorBean() {
        
    }

	@Override
	public boolean adicionarOperador(Operador operador) {
		entityManager.persist(operador);
		return true;
	}

	@Override
	public Collection<Operador> listarOperadores() {
		Query query = entityManager.createQuery("SELECT o FROM Operador o");
	    return (Collection<Operador>) query.getResultList();
	}

	@Override
	public Operador listarOperador(Integer id) {
		return entityManager.find(Operador.class, id);
	}

	@Override
	public boolean editarOperador(Operador operador) {
		entityManager.merge(operador);
		entityManager.flush();
		return true;
	}

	@Override
	public boolean excluirOperador(Operador operador) {
		entityManager.remove(entityManager.getReference(Operador.class, operador.getId()));
		return true;
	}
}
