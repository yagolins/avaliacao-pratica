package br.com.avaliacao.web;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import br.com.avaliacao.bean.OperadorBeanLocal;
import br.com.avaliacao.entity.Operador;

@Path("operador")
@Stateless
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class OperadorRest {
	
	@EJB
	private OperadorBeanLocal operadorBeanLocal;
	
	@Context
    private UriInfo uriInfo;

	public OperadorRest() {
    }
            
    @POST
    public void saveOperador(){      
    	Operador operador = new Operador();
        operador.setNome("Yago");
        operador.setLogin("yago@gmail.com");
        operador.setSenha("123456");
        operador.setDataCadastro(new Date());
        operadorBeanLocal.adicionarOperador(operador);
    }
    
    @GET
    @Path("{id}/")
    public Operador getCustomerById(@PathParam("id") Integer id) {
        return operadorBeanLocal.listarOperador(id);       
    }
        
    @GET
    @Path("{id}/")
    public void deleteCustomer(@PathParam("id") Integer id) {
    	Operador operador = operadorBeanLocal.listarOperador(id);
		operadorBeanLocal.excluirOperador(operador);
    }
    
    @PostConstruct
    public void init() {

    }       
	
}
